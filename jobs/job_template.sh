#!/bin/bash
#SBATCH --time=03:00:00
#SBATCH --mem=256gb
#SBATCH --job-name=aiss_cv
#SBATCH --gres=gpu:2
# above: comments above for sbatch configuration (can be overriden in sbatch cli command)

#
# Conda environment name
# - will use "base" if not specified
# - will create a new environment if the specified environment does not exist
# - configure dependency installation in jobs/conda_config/handle_conda_activation.sh
#
export ENV_NAME="my_env_name"
#
# Configuration variables (simplify configuration of jobs)
#
SOME_VARIABLE=can_be_used_here

# amount of GPU devices to use (specific to GPU usage)
AMOUNT_DEVICES=2
# Generate device IDs list
DEVICE_IDS=$(seq -s "," 0 $((AMOUNT_DEVICES-1)))


#
# CONDA SETUP
#
# make sure conda is available and properly set up
source jobs/conda_config/handle_conda_activation.sh

# Run the command to get the active environment information
env_info=$(conda info --json)

# Check if the environment_info contains the "active_prefix" key
if [[ $env_info == *"active_prefix"* ]]; then
    # Extract the active environment name from the environment_info
    active_env=$(echo "$env_info" | jq -r '.active_prefix' | awk -F'/' '{print $NF}')
    echo "Conda environment '$active_env' is active."
else
    echo "Conda environment is not active."
fi
# end of conda setup

echo "Setup complete. Starting job..."

# job command
python some_script.py --some_parameter $SOME_VARIABLE --device_ids $DEVICE_IDS
